# Adafruit KB2040 Circuitpython

Mostly just my mac Macros.

Added "dark" mode to turn off the display for a macro with an empty name. By combining this with a configuration with dark key caps it appears to turn off in this macro selection (quiet.py)
