# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys: Quicktime for Mac

from adafruit_hid.keycode import Keycode # REQUIRED if using Keycode.* values
from adafruit_hid.consumer_control_code import ConsumerControlCode

app = {                      # REQUIRED dict, must be named 'app'
    'name' : 'Photobooth Dev', # Application name
    'macros' : [             # List of button macros...
        # COLOR    LABEL    KEY SEQUENCE
        # 1st row ----------
        (0x005050, 'Core', [Keycode.COMMAND, ' ', 0.25, 'https://tennis.service-now.com/now/appenginestudio/my-apps/299cd5a447ee1d10552caf52736d430b', Keycode.ENTER]),
        (0x505000, 'App', [Keycode.COMMAND, ' ', 0.25, 'https://tennis.service-now.com/now/build/ui/apps/aa68bcf74742dd10552caf52736d432d', Keycode.ENTER]),
        (0x500050, 'UIC', [Keycode.COMMAND, ' ', 0.25, '> cd ~/dev/photobooth-uic-camera;code .', Keycode.ENTER]),
        # 2nd row ----------
        (0x003030, '     S ', [Keycode.COMMAND, ' ', 0.25, 'https://tennis.service-now.com/$studio.do?sysparm_transaction_scope=299cd5a447ee1d10552caf52736d430b', Keycode.ENTER]),
        (0x303000, 't u d i', [Keycode.COMMAND, ' ', 0.25, 'https://tennis.service-now.com/$studio.do?sysparm_transaction_scope=baa73cb74742dd10552caf52736d43f1', Keycode.ENTER]),
        (0x300030, ' o     ', [Keycode.COMMAND, ' ', 0.25, 'https://tennis.service-now.com/$studio.do?sysparm_transaction_scope=fe8710bf47ce9d10552caf52736d432a', Keycode.ENTER]),                     
        # 3rd row ----------
        (0x001010, '', [Keycode.COMMAND, ' ', 0.25, 'https://github.com/ServiceNowNextExperience/photobooth-core', Keycode.ENTER]),
        (0x101000, 'Github', [Keycode.COMMAND, ' ', 0.25, 'https://github.com/ServiceNowNextExperience/photobooth', Keycode.ENTER]),
        (0x100010, '', [Keycode.COMMAND, ' ', 0.25, 'https://github.com/ServiceNowNextExperience/photobooth-uic-camera', Keycode.ENTER]),                     
        # 4th row ----------
        (0x400000, 'Deploy', ['snc ui-component deploy --force']), 
        (0x101000, '', [Keycode.COMMAND, ' ', 0.25, 'https://tennis.service-now.com/now/photobooth/home/MyPB', Keycode.ENTER]),   
        (0x004000, 'Develop', ['snc ui-component develop --open', Keycode.ENTER]), 
        # Encoder button ---
        (0x000000, '', [Keycode.COMMAND, ' ', 0.25, '> code ~/dev'])
    ]
}
