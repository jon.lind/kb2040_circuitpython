# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys example: Web browsers for Windows

from adafruit_hid.keycode import Keycode # REQUIRED if using Keycode.* values

app = {                      # REQUIRED dict, must be named 'app'
    'name' : 'Windows Browser', # Application name
    'macros' : [             # List of button macros...
        # COLOR    LABEL    KEY SEQUENCE
        # 1st row ----------
        (0x004000, '< Back', [Keycode.ALT, Keycode.LEFT_ARROW]),
        (0x004000, 'Fwd >', [Keycode.ALT, Keycode.RIGHT_ARROW]),
        (0x400000, 'Up', [Keycode.PAGE_UP]),      # Scroll up
        # 2nd row ----------
        (0x202000, '< Tab', [Keycode.CONTROL, Keycode.PAGE_UP]),
        (0x202000, 'Tab >', [Keycode.CONTROL, Keycode.PAGE_DOWN]),
        (0x400000, 'Down', [Keycode.PAGE_DOWN]),                     # Scroll down
        # 3rd row ----------
        (0x202000, 'Tab 1', [Keycode.CONTROL, '1']), # Tab 1
        (0x333333, 'Reload', [Keycode.F5]),
        (0x000040, 'Home', [Keycode.ALT, Keycode.HOME]),
        # 4th row ----------
        (0x400000, 'X App', [Keycode.ALT, Keycode.F4]), # Close App
        (0x440044, 'Desktop', [Keycode.COMMAND, 'd']),   # Show desktop
        (0x400000, 'X Win', [Keycode.CONTROL, Keycode.F4]), # Close Window/Tab
        # Encoder button ---
        (0x000000, '', [Keycode.ALT, Keycode.F4]) # Close App
    ]
}
