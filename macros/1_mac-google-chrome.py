# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys example: Google Chrome web browser for Mac

from adafruit_hid.keycode import Keycode # REQUIRED if using Keycode.* values

app = {                    # REQUIRED dict, must be named 'app'
    'name' : 'Mac Browser', # Application name
    'macros' : [           # List of button macros...
        # COLOR    LABEL    KEY SEQUENCE
        # 1st row ----------
        (0x004000, '< Back', [Keycode.COMMAND, '[']),
        (0x004000, 'Fwd >', [Keycode.COMMAND, ']']),
        (0x400000, 'Up', [Keycode.PAGE_UP]),      # Scroll up
        # 2nd row ----------
        (0x202000, '< Tab', [Keycode.CONTROL, Keycode.SHIFT, Keycode.TAB]),
        (0x202000, 'Tab >', [Keycode.CONTROL, Keycode.TAB]),
        (0x400000, 'Down', [Keycode.PAGE_DOWN]),                     # Scroll down
        # 3rd row ----------
        (0x202000, 'Tab 1', [Keycode.COMMAND, '1']), # Tab 1
        (0x333333, 'Reload', [Keycode.COMMAND, 'r']),
        (0x000040, 'Home', [Keycode.COMMAND, 'H']),
        # 4th row ----------
        (0x440044, 'Tech', [Keycode.ALT, Keycode.SHIFT, '7']), # ServiceNow Technical Names (Arnoodkooi's SN Utils Chrome Extensions) 
        (0x440044, 'Slash', [Keycode.ALT, Keycode.SHIFT, '0']), # Slashcommand/shortcut Popup (Arnoodkooi's SN Utils Chrome Extensions) 
        (0x400000, 'X Win', [Keycode.COMMAND, 'w']), # Close Window/Tab
        # Encoder button ---
        (0x000000, '', ['jon.lind@servicenow.com']) 
    ]
}
